const url = process.env.APPLICATION_LOG_CONNECTION_STRING || 'amqp://logUser:logPwd@localhost:5672/mtntLogger';
const queue = process.env.APPLICATION_LOG_QUEUE_DEFAULT || 'nodeMtntLog';

exports.log = (msg) => {

    const open = require('amqplib').connect(url);

    open.then(conn => {
      return conn.createChannel();
    })
    .then(channel => {
      return channel.assertQueue(queue).then(ok => {
        return channel.sendToQueue(queue, Buffer.from(msg));
      });
    })
    .catch(console.warn);
}