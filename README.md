# Node Api Mtnt

## Aplicção de teste de conhecimento em JavaScript ES6 e Node.js

### Stack da aplicação

- Node.js
- Expressjs
- RabbitMQ
- ELK stack
- Docker

### Rodando a aplicação usando Docker

```
chmod +x run.sh
./run.sh --build

```

### Consumindo os dados externos

```
http://localhost:8080

```
### Filtrando os sites dos usuarios

```
http://localhost:8080/sites

```
### Visualizar os usuarios em ordem alfabetica

```
http://localhost:8080/users

```
### Filtrando os usuarios com suite no endereço

```
http://localhost:8080/suiteusers

```

### Rodando os testes

```
npm test

```