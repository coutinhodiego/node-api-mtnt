const express = require('express');

const routes = require('./routes/routes')
const PORT = process.env.PORT || 3000

const app = express();

app.use('/', routes)

app.listen(PORT, () => {
    console.log(`LISTEN ON PORT : ${PORT}`)
});

module.exports = app;
