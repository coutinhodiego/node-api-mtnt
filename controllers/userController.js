const httpRequest = require('../helpers/httpRequest')
const Log = require('../amqplib/rabbitmq')

const url = 'https://jsonplaceholder.typicode.com/users'

exports.loadSites = (req, res, next) => {

    httpRequest.get(url, result => {

        let sites = this.forElements(result)

        sites.forEach(site => Log.log(site))

        return res.status(200).json({
            status: 200,
            title: 'Os websites de todos os usuários',
            users: sites
        });
    })
}

exports.userData = (req, res, next) => {

    httpRequest.get(url, result => {

        let orderedUsers = this.orderDataResult(result)
        orderedUsers.forEach(user => Log.log(JSON.stringify(user)))

        return res.status(200).json({
            status: 200,
            title: 'Lista dos usuários',
            users: orderedUsers
        });
    });
}

exports.showSuiteUsers = (req, res, next) => {

        httpRequest.get(url, result => {
            
            let suiteUsers = this.filterDataResult(result)

            suiteUsers.forEach(user => Log.log(JSON.stringify(user)))

            return res.status(200).json({
                status: 200,
                title: 'Lista dos usuários com suite',
                users: suiteUsers
            });
        })



}

exports.forElements = (array) => {

    return array.map(element => {
        return element.website;
    }).sort();

}

exports.orderDataResult = (array) => {

    return array.map(element => {
        return {
            nome: element.name,
            email: element.email,
            empresa: element.company.name
        }
    })
    .sort((a, b) => {
        return a.nome > b.nome ? 1 : b.nome > a.nome ? -1 : 0;
    })
}

exports.filterDataResult = (array) => {

    return array.filter(user => {
        return user.address.suite.split(' ')[0] == 'Suite'
    })

}