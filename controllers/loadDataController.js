const httpRequest = require('../helpers/httpRequest')

const url = 'https://jsonplaceholder.typicode.com/users'

exports.loadUsers = (req, res, next) => {
    
    httpRequest.get(url, (data) => {

            return res.status(200).json({
                status: 200,
                title: 'Carregando usuarios',
                users: data
            });
        });
};

