const express = require('express')
const router = express.Router()

const userController = require('../controllers/userController')
const loadDataController = require('../controllers/loadDataController')

router.get('/', loadDataController.loadUsers)
router.get('/sites', userController.loadSites)
router.get('/users', userController.userData)
router.get('/suiteusers', userController.showSuiteUsers)

module.exports = router;
