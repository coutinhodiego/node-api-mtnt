FROM node:alpine

MAINTAINER Diego Coutinho

ENV PORT=3000

COPY . /node/api

EXPOSE $PORT

WORKDIR /node/api

RUN npm install

CMD ["npm", "start"]
