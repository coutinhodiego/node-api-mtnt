const chai = require('chai');
const expect = chai.expect;

const suiteUsers = require('./suiteUsers.json')
const orderedSites = require('./orderedSites.json')
const orderedUsers = require('./orderedUsers.json')
const userData = require('./data.json')

const userController = require('../controllers/userController')

describe('Users Controller', () => {

  it('should extract site field about all users', done => {

      const sites = userController.forElements(userData)
      expect(sites).to.have.ordered.members(orderedSites);
      done();

  })

  it('should organize users in alphabetical order', done => {

    const users = userController.orderDataResult(userData)
    expect(users).to.have.ordered.deep.members(orderedUsers);
    done();

  })

  it('should filter users with suite name in addrress', done => {

    const addressUsers = userController.filterDataResult(userData)
    expect(addressUsers).to.have.deep.members(suiteUsers);
    done();

  })

})