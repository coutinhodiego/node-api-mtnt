const https = require('https');

exports.get = (url, callback) => {
    
    https.get(url, result => {

        let allData = '';

        result.on('data', data => {
            allData += data;
        });
        

        result.on('end', () => {
            return callback(JSON.parse(allData));
        });
    });
};